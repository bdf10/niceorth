from sage.all import cartesian_product_iterator, vector


def find_vectors(d, r):
    # This function finds all vectors of dimension d
    # with nonnegative integer coordinates such that
    # the dot product of the vector with itself is less than or equal to r.

    points = map(vector, cartesian_product_iterator([range(r+1) for _ in range(d)]))
    return list(filter(lambda v: v*v <= r, points))


if __name__ == '__main__':
    d = 103
    r = 5
    print(find_vectors(d, r))
