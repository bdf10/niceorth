from collections import namedtuple
from sage.all import polytopes, Polyhedron, ceil, sqrt, identity_matrix, gcd


N_Squares_Data = namedtuple('N_Squares_Data', 'Number FourSquares FourSquaresPrimitive')


def is_primitive(v):
    return gcd(v) == 1


def m_squares_data(n=1, m=4):
    C = ceil(sqrt(n))*polytopes.hypercube(m)
    PP = Polyhedron(rays=identity_matrix(m).rows())
    P = C.intersection(PP)
    vs = [v for v in P.integral_points() if v*v == n]
    vs_primitive = list(filter(is_primitive, vs))
    return N_Squares_Data(n, vs, vs_primitive)


def bad_m_square(m=4, bound=100):
    for n in range(1,bound):
        print(f'testing n={n}')
        if not m_squares_data(n, m=m)[-1]:
            print(f'n={n} is bad!!!')
