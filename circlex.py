from sage.all import polytopes, sqrt, plot, sphere


def circlex(dim=2, r_squared=19, error=1/10):
    simplex = polytopes.simplex(dim-1)
    l = sqrt(r_squared).n().nearby_rational(max_error=error)-2*error
    u = sqrt(dim*r_squared).n().nearby_rational(max_error=error)+2*error
    return simplex.dilation(l), simplex.dilation(u)


def plot_circlex(dim=3, r_squared=19, error=1/10):
    p1, p2 = circlex(dim=dim, r_squared=r_squared, error=error)
    return plot(p1.convex_hull(p2))+plot(sphere((0,0,0), sqrt(r_squared)))
