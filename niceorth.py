from sage.all import (polytopes,
                      Polyhedron,
                      ceil,
                      choice,
                      sqrt,
                      matrix,
                      gcd)


hypercube = polytopes.hypercube


def niceorth(m=4, n=3, d=7, verbose=None):
    C = hypercube(m, intervals='zero_one').dilation(ceil(sqrt(d)))
    all_pts = [v for v in C.integral_points() if v*v == d]
    primitive_pts = list(filter(lambda v: gcd(v) == 1, all_pts))
    pts = primitive_pts + bool(not primitive_pts)*all_pts
    verbose and print(f'Found {len(pts)} options for column 1 (in + orthant)')
    A = matrix.column([choice([-1, 1])*_ for _ in choice(pts)])
    C = hypercube(m).dilation(ceil(sqrt(d)))
    while A.ncols() != n:
        P = Polyhedron(lines=A.left_kernel().basis())
        pts = [v for v in P.intersection(C).integral_points() if v*v == d]
        verbose and print(f'Found {len(pts)} options for column {A.ncols()+1}')
        A = A.augment(choice(pts))
    return A
